package routing.community;

import java.util.*;

import core.*;
import routing.DecisionEngineRouter;
import routing.MessageRouter;
import routing.RoutingDecisionEngine;
import static routing.community.SimBet.CENTRALITY_ALG_SETTING;

public class DistributedBubbleRapwBtwns 
implements RoutingDecisionEngine, CommunityDetectionEngine
{
	public static final String COMMUNITY_ALG_SETTING = "communityDetectAlg";
	public static final String CENTRALITY_ALG_SETTING = "centralityAlg";
	public static final String CENTRALITY_ALG_SETTING2 = "centralityAlg2";
	
	protected Map<DTNHost, Double> startTimestamps;
	protected Map<DTNHost, List<Duration>> connHistory;
	
	protected CommunityDetection community;
	protected Centrality centrality;
        
        //---------------------------------------------------------------------------------------
        protected double betweennessCentrality;// menyimpan nilai betweenness centrality
        protected double[][] matrixEgoNetwork; // menyimpan nilai matrix ego network
        protected Map<DTNHost, Set<DTNHost>> neighboursNode;
        protected CentralityDetection centrality2;
        
	
	public DistributedBubbleRapwBtwns(Settings s)
	{
            if (s.contains(CENTRALITY_ALG_SETTING2))
			 this.centrality2 = (CentralityDetection) s.createIntializedObject(s.getSetting(CENTRALITY_ALG_SETTING2));
		 else
			 this.centrality2 = new BetweennessCentrality(s);
		if(s.contains(COMMUNITY_ALG_SETTING))
			this.community = (CommunityDetection) 
				s.createIntializedObject(s.getSetting(COMMUNITY_ALG_SETTING));
		else
			this.community = new SimpleCommunityDetection(s);
		
		if(s.contains(CENTRALITY_ALG_SETTING))
			this.centrality = (Centrality) 
				s.createIntializedObject(s.getSetting(CENTRALITY_ALG_SETTING));
		else
			this.centrality = new SWindowCentrality(s);
	}
	
	public DistributedBubbleRapwBtwns(DistributedBubbleRapwBtwns proto)
	{
		this.community = proto.community.replicate();
		this.centrality = proto.centrality.replicate();                		
		this.centrality2 = proto.centrality2.replicate();
                neighboursNode = new HashMap<DTNHost, Set<DTNHost>>();
		startTimestamps = new HashMap<DTNHost, Double>();
		connHistory = new HashMap<DTNHost, List<Duration>>();
	}

	public void connectionUp(DTNHost thisHost, DTNHost peer){}

	public void doExchangeForNewConnection(Connection con, DTNHost peer)
	{
		DTNHost myHost = con.getOtherNode(peer);
		DistributedBubbleRapwBtwns de = this.getOtherDecisionEngine(peer);
		if (this.neighboursNode.containsKey(peer)) { // jika node sudah pernah bertemu sebelumnya

			// daftar tetangga dari node peer akan diperbarui
			de.neighboursNode.replace(myHost, this.neighboursNode.keySet());
			this.neighboursNode.replace(peer, de.neighboursNode.keySet());
		}

		else { // jika node baru pertama kali ditemui

			// node baru akan ditambahkan ke dalam daftar tetangga
			// beserta tetangga yang sudah ditemui node peer
			de.neighboursNode.put(myHost, this.neighboursNode.keySet());
			this.neighboursNode.put(peer, de.neighboursNode.keySet());
		}
                
		this.startTimestamps.put(peer, SimClock.getTime());
		de.startTimestamps.put(myHost, SimClock.getTime());
		
		this.community.newConnection(myHost, peer, de.community);
                this.updateBetweenness(myHost); // mengupdate nilai betweenness
	}
	
	public void connectionDown(DTNHost thisHost, DTNHost peer)
	{
		double time = startTimestamps.get(peer);
		double etime = SimClock.getTime();
		
		// Find or create the connection history list
		List<Duration> history;
		if(!connHistory.containsKey(peer))
		{
			history = new LinkedList<Duration>();
			connHistory.put(peer, history);
		}
		else
			history = connHistory.get(peer);
		
		// add this connection to the list
		if(etime - time > 0)
			history.add(new Duration(time, etime));
		
		CommunityDetection peerCD = this.getOtherDecisionEngine(peer).community;
		
		// inform the community detection object that a connection was lost.
		// The object might need the whole connection history at this point.
		community.connectionLost(thisHost, peer, peerCD, history);
		
		startTimestamps.remove(peer);
	}

	public boolean newMessage(Message m)
	{
		return true; // Always keep and attempt to forward a created message
	}

	public boolean isFinalDest(Message m, DTNHost aHost)
	{
		return m.getTo() == aHost; // Unicast Routing
	}

	public boolean shouldSaveReceivedMessage(Message m, DTNHost thisHost)
	{
		return m.getTo() != thisHost;
	}

	public boolean shouldSendMessageToHost(Message m, DTNHost otherHost)
	{
		if(m.getTo() == otherHost) return true; // trivial to deliver to final dest
		
		
		DTNHost dest = m.getTo();
		DistributedBubbleRapwBtwns de = getOtherDecisionEngine(otherHost);
		
		// Which of us has the dest in our local communities, this host or the peer
		boolean peerInCommunity = de.commumesWithHost(dest);
		boolean meInCommunity = this.commumesWithHost(dest);
		
		if(peerInCommunity && !meInCommunity) // peer is in local commun. of dest
			return true;
		else if(!peerInCommunity && meInCommunity) // I'm in local commun. of dest
			return false;
		else if(peerInCommunity) // we're both in the local community of destination
		{
			// Forward to the one with the higher local centrality (in our community)
			if(de.getLocalCentrality() > this.getLocalCentrality())
				return true;
			else
				return false;
		}
		// Neither in local community, forward to more globally central node
		else if(de.getBetweennessCentrality()> this.getBetweennessCentrality())
			return true;
		
		return false;
	}

	public boolean shouldDeleteSentMessage(Message m, DTNHost otherHost)
	{
                DistributedBubbleRapwBtwns de = this.getOtherDecisionEngine(otherHost);
                return de.commumesWithHost(m.getTo()) &&
			!this.commumesWithHost(m.getTo());
	}

	public boolean shouldDeleteOldMessage(Message m, DTNHost hostReportingOld)
	{
		DistributedBubbleRapwBtwns de = this.getOtherDecisionEngine(hostReportingOld);
		return de.commumesWithHost(m.getTo()) && 
			!this.commumesWithHost(m.getTo());
	}

	public RoutingDecisionEngine replicate()
	{
		return new DistributedBubbleRapwBtwns(this);
	}
	
	protected boolean commumesWithHost(DTNHost h)
	{
		return community.isHostInCommunity(h);
	}
	
	protected double getLocalCentrality()
	{
		return this.centrality.getLocalCentrality(connHistory, community);
	}
	
	protected double getGlobalCentrality()
	{
		return this.centrality.getGlobalCentrality(connHistory);
	}

	private DistributedBubbleRapwBtwns getOtherDecisionEngine(DTNHost h)
	{
		MessageRouter otherRouter = h.getRouter();
		assert otherRouter instanceof DecisionEngineRouter : "This router only works " + 
		" with other routers of same type";
		
		return (DistributedBubbleRapwBtwns) ((DecisionEngineRouter)otherRouter).getDecisionEngine();
	}

	public Set<DTNHost> getLocalCommunity() {return this.community.getLocalCommunity();}
	
        
        
        //-----------------------------------------------
        public double getBetweennessCentrality() {
		return this.betweennessCentrality;
	}
        protected void updateBetweenness(DTNHost myHost) {
		this.buildEgoNetwork(this.neighboursNode, myHost); // membangun ego network
		this.betweennessCentrality = this.centrality2.getCentrality(this.matrixEgoNetwork); //menghitung nilai betweenness centrality
	}
        
        	protected void buildEgoNetwork(Map<DTNHost, Set<DTNHost>> neighboursNode, DTNHost host) {
		ArrayList<DTNHost> dummyArray = buildDummyArray(neighboursNode, host);

		double[][] neighboursAdj = new double[dummyArray.size()][dummyArray.size()];

		for (int i = 0; i < dummyArray.size(); i++) {
			for (int j = i; j < dummyArray.size(); j++) {
				if (i == j) {
					neighboursAdj[i][j] = 0;
				} else if (neighboursNode.get(dummyArray.get(j)).contains(dummyArray.get(i))) {
					neighboursAdj[i][j] = 1;
					neighboursAdj[j][i] = neighboursAdj[i][j];
				} else {
					neighboursAdj[i][j] = 0;
					neighboursAdj[j][i] = neighboursAdj[i][j];
				}
			}
		}

		this.matrixEgoNetwork = neighboursAdj;
	}

	protected ArrayList<DTNHost> buildDummyArray(Map<DTNHost, Set<DTNHost>> neighbours, DTNHost myHost) {
		ArrayList<DTNHost> dummyArray = new ArrayList<>();
		dummyArray.add(myHost);
		dummyArray.addAll(neighbours.keySet());
		return dummyArray;
	}
}