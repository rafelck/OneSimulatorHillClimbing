
/*
 * Copyright (c) 2018. Improving SimBet algorithm
 * using Delegation Forwarding.
 * author Rafelino Claudius Kelen, Sanata Dharma University.
 */
package routing.community;

import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;
import routing.DecisionEngineRouter;
import routing.MessageRouter;

import java.util.*;
import routing.RoutingDecisionEngine;

public class SimBetSAbufferControl implements RoutingDecisionEngine, SimilarityDetectionEngine {

    public static final String CENTRALITY_ALG_SETTING = "centralityAlg";
    public static final String SIMILARITY_SETTING = "similarityAlg";
    public static final String A_SETTING = "a";
    public static final String T_SETTING = "t";
    public static final String TMIN_SETTING = "t_min";

    protected Map<DTNHost, Set<DTNHost>> neighboursNode; // menyimpan daftar tetangga dari ego node
    protected Map<String, Double> mUtil; //menyimpan message id,dan utility node

    protected double[][] matrixEgoNetwork; // menyimpan nilai matrix ego network
    protected double[][] indirectNodeMatrix; //menyimpan matrix indirect node

    protected double betweennessCentrality;// menyimpan nilai betweenness centrality
    protected double a; //menyimpan konstanta untuk variabel similarity
    protected double b = 1 - a; //menyimpan konstanta untuk variabel betweenness
    protected double t;
    protected double t_min;
    double ap = 1;

    ArrayList<DTNHost> indirectNode, directNode; //menyimpan indirect node => m dan direct node+host => n

    protected SimilarityCounter similarity;
    protected CentralityDetection centrality;
    double mySimbetUtil;
    double peerSimBetUtil;

    public SimBetSAbufferControl(Settings s) {

        if (s.contains(CENTRALITY_ALG_SETTING)) {
            this.centrality = (CentralityDetection) s.createIntializedObject(s.getSetting(CENTRALITY_ALG_SETTING));
        } else {
            this.centrality = new BetweennessCentrality(s);
        }

        if (s.contains(SIMILARITY_SETTING)) {
            this.similarity = (SimilarityCounter) s.createIntializedObject(s.getSetting(SIMILARITY_SETTING));
        } else {
            this.similarity = new NeighbourhoodSimilarity(s);
        }

        this.a = s.getDouble(A_SETTING);
        this.t = s.getDouble(T_SETTING);
        this.t_min = s.getDouble(TMIN_SETTING);
    }

    public SimBetSAbufferControl(SimBetSAbufferControl proto) {
        neighboursNode = new HashMap<DTNHost, Set<DTNHost>>();
        indirectNode = new ArrayList<DTNHost>();
        directNode = new ArrayList<DTNHost>();
        mUtil = new HashMap<String, Double>();
        this.a = proto.a;
        this.t = proto.t;
        this.t_min = proto.t_min;
        this.centrality = proto.centrality.replicate();
        this.similarity = proto.similarity.replicate();
    }

    @Override
    public void connectionUp(DTNHost thisHost, DTNHost peer) {
//         System.out.println("Before --------- Node "+thisHost+" sisa buffer "+thisHost.getBufferOccupancy());
    }

    @Override
    public void connectionDown(DTNHost thisHost, DTNHost peer) {
//        System.out.println("After  ########## Node "+thisHost+" sisa buffer "+thisHost.getBufferOccupancy());
    }

    @Override
    public void doExchangeForNewConnection(Connection con, DTNHost peer) {

        DTNHost myHost = con.getOtherNode(peer);
        SimBetSAbufferControl de = this.getOtherDecisionEngine(peer);

        if (this.neighboursNode.containsKey(peer)) { // jika node sudah pernah bertemu sebelumnya

            // daftar tetangga dari node peer akan diperbarui
            de.neighboursNode.replace(myHost, this.neighboursNode.keySet());
            this.neighboursNode.replace(peer, de.neighboursNode.keySet());
        } else { // jika node baru pertama kali ditemui

            // node baru akan ditambahkan ke dalam daftar tetangga
            // beserta tetangga yang sudah ditemui node peer
            de.neighboursNode.put(myHost, this.neighboursNode.keySet());
            this.neighboursNode.put(peer, de.neighboursNode.keySet());
        }

        this.updateBetweenness(myHost); // mengupdate nilai betweenness
        this.updateSimilarity(myHost); //mengupdate indirect node 
    }

    @Override
    public boolean shouldSendMessageToHost(Message m, DTNHost otherHost) {

        SimBetSAbufferControl de = getOtherDecisionEngine(otherHost);
        DTNHost dest = m.getTo();

        if (isFinalDest(m, otherHost)) {
            return true;
        }
        //hitung nilai simbet util saya
        mySimbetUtil = this.countSimBetUtil(de.getSimilarity(dest), de.getBetweennessCentrality(),
                this.getSimilarity(dest), this.getBetweennessCentrality());

        //hitung nilai simbet util teman saya
        peerSimBetUtil = this.countSimBetUtil(this.getSimilarity(dest), this.getBetweennessCentrality(),
                de.getSimilarity(dest), de.getBetweennessCentrality());
        /*routing dengan kombinasi similarity & betweenness*/
        if (peerSimBetUtil > mySimbetUtil) {
            if (otherHost.getBufferOccupancy() / 100 >= 0.5) 
                return false;
             else 
                return true;
        } else {
            if (peerSimBetUtil > ap) 
                return true;
             else {
                if (peerSimBetUtil < ap) {
                    if (this.t >= this.t_min) {
                        double countSa = Math.exp(-1 / this.t * (mySimbetUtil - peerSimBetUtil));
                        if (countSa > Math.random()) {
                            ap = countSa;
                        }
                        this.t = this.t * 0.9;
                    }
                }
            }
            return false;
        }
    }

    // ambil nilai similarity ke node dest
    @Override
    public double getSimilarity(DTNHost dest) {

        int index = 0; //digunakan untuk membantu penghitungan index

        //cek apakah node dest merupakan direct node
        if (this.directNode.contains(dest)) {
            for (DTNHost dtnHost : this.directNode) {

                if (dtnHost == dest) {
                    return this.similarity.countSimilarity(this.matrixEgoNetwork, null, index);
                }
                index++;
            }
        }

        //cek apakah node dest merupakan indirect node
        if (this.indirectNode.contains(dest)) {

            //bangun matrix adjacency indirect node
            this.buildIndirectNodeMatrix(this.neighboursNode, dest);

            //hitung nilai similarity
            return this.similarity.countSimilarity(this.matrixEgoNetwork, this.indirectNodeMatrix, 0);

        }

        return 0;
    }

    // mengambil nilai betweenness yang sudah dihitung
    public double getBetweennessCentrality() {
        return this.betweennessCentrality;
    }

    // mengupdate nilai betweenness centrality
    protected void updateBetweenness(DTNHost myHost) {
        this.buildEgoNetwork(this.neighboursNode, myHost); // membangun ego network
        this.betweennessCentrality = this.centrality.getCentrality(this.matrixEgoNetwork); //menghitung nilai betweenness centrality
    }

    protected void updateSimilarity(DTNHost myHost) {

        //simpan data indirect node
        this.indirectNode.addAll(this.searchIndirectNeighbours(this.neighboursNode));

    }

    protected Set<DTNHost> searchIndirectNeighbours(Map<DTNHost, Set<DTNHost>> neighboursNode) {

        // mengambil daftar tetangga yang sudah ditemui secara langsung
        Set<DTNHost> directNeighbours = neighboursNode.keySet();

        // variabel untuk menyimpan daftar node yang belum pernah ditemui secara
        // langsung
        Set<DTNHost> setOfIndirectNeighbours = new HashSet<>();

        for (DTNHost dtnHost : directNeighbours) {

            // mengambil daftar tetangga dari peer yang sudah ditemui langsung
            Set<DTNHost> neighboursOfpeer = neighboursNode.get(dtnHost);

            for (DTNHost dtnHost1 : neighboursOfpeer) {

                // jika dtnHost1 belum pernah ditemui secara langsung
                if (!directNeighbours.contains(dtnHost1)) {

                    // cek apakah listOfUndirectNeighbours masih kosong
                    if (setOfIndirectNeighbours.isEmpty()) {

                        // jika masih kosong masukkan langsung dtnHost1 ke dalam
                        // listOfIndirectNeighbours
                        setOfIndirectNeighbours.add(dtnHost1);

                    } else {// jika listOfUndirectNeighbours tidak kosong

                        // cek apakah dtnHost1 sudah pernah dicatat ke dalam
                        // listOfindirectNeighbours
                        if (!setOfIndirectNeighbours.contains(dtnHost1)) {
                            setOfIndirectNeighbours.add(dtnHost1);
                        }
                    }
                }
            }
        }

        return setOfIndirectNeighbours;
    }

    // method yang digunakan untuk membangun matriks ego network
    public void buildIndirectNodeMatrix(Map<DTNHost, Set<DTNHost>> neighboursNode, DTNHost dest) {
        ArrayList<DTNHost> dummyArrayN = this.directNode;

        double[][] neighboursAdj = new double[dummyArrayN.size()][1];

        for (int i = 0; i < dummyArrayN.size(); i++) {
            for (int j = 0; j < 1; j++) {
                if (i == 0) {
                    neighboursAdj[i][j] = 0;
                } else if (neighboursNode.get(dummyArrayN.get(i)).contains(dest)) {
                    neighboursAdj[i][j] = 1;

                } else {
                    neighboursAdj[i][j] = 0;

                }
            }
        }

        this.indirectNodeMatrix = neighboursAdj;
    }

    // method yang digunakan untuk membangun matriks ego network
    protected void buildEgoNetwork(Map<DTNHost, Set<DTNHost>> neighboursNode, DTNHost host) {
        ArrayList<DTNHost> dummyArray = buildDummyArray(neighboursNode, host);

        double[][] neighboursAdj = new double[dummyArray.size()][dummyArray.size()];

        for (int i = 0; i < dummyArray.size(); i++) {
            for (int j = i; j < dummyArray.size(); j++) {
                if (i == j) {
                    neighboursAdj[i][j] = 0;
                } else if (neighboursNode.get(dummyArray.get(j)).contains(dummyArray.get(i))) {
                    neighboursAdj[i][j] = 1;
                    neighboursAdj[j][i] = neighboursAdj[i][j];
                } else {
                    neighboursAdj[i][j] = 0;
                    neighboursAdj[j][i] = neighboursAdj[i][j];
                }
            }
        }

        this.matrixEgoNetwork = neighboursAdj;
    }

    protected ArrayList<DTNHost> buildDummyArray(Map<DTNHost, Set<DTNHost>> neighbours, DTNHost myHost) {
        ArrayList<DTNHost> dummyArray = new ArrayList<>();
        dummyArray.add(myHost);
        dummyArray.addAll(neighbours.keySet());
        this.directNode = dummyArray; //mengisi himpunan n pada matrix 
        return dummyArray;
    }

    public double countSimBetUtil(double simPeerForDest, double betweennessPeer, double mySimForDest, double myBetweenness) {
        double simBetUtil;
        double simUtilForDest, betUtil;
        simUtilForDest = mySimForDest / (mySimForDest + simPeerForDest);

        betUtil = myBetweenness / (myBetweenness + betweennessPeer);

        simBetUtil = (this.a * simUtilForDest) + ((1 - this.a) * betUtil);

        return simBetUtil;

    }

    public double getMyUtility() {
        return this.mySimbetUtil;

    }

    public double getOtherUtility() {
        return this.peerSimBetUtil;

    }

    @Override
    public boolean newMessage(Message m) {
        return true;
    }

    @Override
    public boolean isFinalDest(Message m, DTNHost aHost) {
        return m.getTo() == aHost;
    }

    @Override
    public boolean shouldSaveReceivedMessage(Message m, DTNHost thisHost) {
        return m.getTo() != thisHost;
    }

    public SimBetSAbufferControl getOtherDecisionEngine(DTNHost otherHost) {
        MessageRouter otherRouter = otherHost.getRouter();
        assert otherRouter instanceof DecisionEngineRouter : "This router only works "
                + " with other routers of same type";

        return (SimBetSAbufferControl) ((DecisionEngineRouter) otherRouter).getDecisionEngine();
    }

    @Override
    public boolean shouldDeleteSentMessage(Message m, DTNHost otherHost) {
        return false;
    }

    @Override
    public boolean shouldDeleteOldMessage(Message m, DTNHost hostReportingOld) {
        return false;
    }

    @Override
    public RoutingDecisionEngine replicate() {
        return new SimBetSAbufferControl(this);
    }

}
