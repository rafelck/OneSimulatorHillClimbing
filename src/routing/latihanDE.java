/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package routing;

import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;

/**
 *
 * @author Dell
 */
public class latihanDE implements RoutingDecisionEngine {

    public latihanDE(Settings s) {
    }

    public latihanDE(latihanDE proto) {
    }
   
    
 
    @Override
    public void connectionUp(DTNHost thisHost, DTNHost peer) {
        System.out.println(thisHost+" connection up");
    }

    @Override
    public void connectionDown(DTNHost thisHost, DTNHost peer) {
        System.out.println("connection down");
    }

    @Override
    public void doExchangeForNewConnection(Connection con, DTNHost peer) {
        DTNHost thisHost = con.getOtherNode(peer);
        System.out.println(thisHost+" doExchange with "+peer);
    }

    @Override
    public boolean newMessage(Message m) {
       
        System.out.println("new m "+m.getFrom()+ "  " +m.getId() );
       return true;
       
    }

    @Override
    public boolean isFinalDest(Message m, DTNHost aHost) {
        System.out.println("is final dest" +aHost+ " msg id = " +m.getId());
        return m.getTo()== aHost;
    }

    @Override
    public boolean shouldSaveReceivedMessage(Message m, DTNHost thisHost) {
        
        System.out.println(thisHost+"  shld sv rcv m");
        return true;
    }

    @Override
    public boolean shouldSendMessageToHost(Message m, DTNHost otherHost) {
        System.out.println("shld snd m to host  "+otherHost);
       return true;
    }

    @Override
    public boolean shouldDeleteSentMessage(Message m, DTNHost otherHost) {
        System.out.println("shld delete sent m  "+otherHost);
        return false;
    }

    @Override
    public boolean shouldDeleteOldMessage(Message m, DTNHost hostReportingOld) {
        System.out.println("shld delete old m  "+m.getId()+"========= "+hostReportingOld );
        return true;
    }

    @Override
    public RoutingDecisionEngine replicate() {
       return new latihanDE(this);
    }
    
}
