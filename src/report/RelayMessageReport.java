/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import core.DTNHost;
import core.Message;
import core.MessageListener;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rafel
 */
public class RelayMessageReport extends Report implements MessageListener {

    protected Map<DTNHost, Integer> relayCounts;

    public RelayMessageReport() {
        init();
    }

    @Override
    public void messageTransferred(Message m, DTNHost from, DTNHost to, boolean firstDelivery) {
        if (relayCounts.containsKey(from)) {
            relayCounts.put(from, relayCounts.get(from) + 1);
        } else {
            relayCounts.put(from, 1);
        }
    }

    @Override
    protected void init() {
        super.init();
        relayCounts = new HashMap<DTNHost, Integer>();
    }

    @Override
    public void done() {
        for (Map.Entry<DTNHost, Integer> entry : relayCounts.entrySet()) {
            DTNHost a = entry.getKey();
            Integer b = a.getAddress();

            write("" + b + '\t' + entry.getValue());
        }
        super.done();
    }

    @Override
    public void newMessage(Message m) {
    }

    @Override
    public void messageTransferStarted(Message m, DTNHost from, DTNHost to) {
    }

    @Override
    public void messageDeleted(Message m, DTNHost where, boolean dropped) {
    }

    @Override
    public void messageTransferAborted(Message m, DTNHost from, DTNHost to) {
    }

    @Override
    public void messageSaveToBuffer(Message m, DTNHost to) {
    }

    @Override
    public void connectionUp(DTNHost thisHost) { }

}
