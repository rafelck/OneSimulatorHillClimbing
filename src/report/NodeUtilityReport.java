package report;

import core.DTNHost;
import core.Message;
import core.MessageListener;
import routing.DecisionEngineRouter;
import routing.MessageRouter;
import routing.community.*;


import java.util.*;

public class NodeUtilityReport extends Report implements MessageListener {
    protected Map<DTNHost, List<Double>> avgUtil = new HashMap<>();

    public NodeUtilityReport()
    {
        init();

    }
    @Override
    public void messageTransferred(Message m, DTNHost from, DTNHost to, boolean firstDelivery) {
        MessageRouter otherRouter = from.getRouter();
        SimBetSA sim = (SimBetSA) ((DecisionEngineRouter) otherRouter).getDecisionEngine();
        double  other = sim.getOtherUtility();

                if (avgUtil.containsKey(to)) {
                    avgUtil.get(to).add(other);

                } else {
                    List<Double> nodeUtil = new ArrayList<>();
                    nodeUtil.add(other);
                    avgUtil.put(to, nodeUtil);

                }
    }
    @Override
    public void done()
    {

        Map<DTNHost, Double> results = new HashMap<>();

            for (Map.Entry<DTNHost, List<Double>> entry : avgUtil.entrySet()) {

                Double average = calcAverage((entry.getValue()));
                results.put(entry.getKey(), average);
                write("" + entry.getKey() + '\t' + average + " ");
        }
        super.done();
    }

    private double calcAverage(List<Double> values) {
        double result = 0;
        for (Double value : values) {
            if (value.isNaN()){
                result += 0;
            }else{
                result += value;
            }
        }
        return result / values.size();
    }

    @Override
    public void newMessage(Message m) {

    }

    @Override
    public void messageTransferStarted(Message m, DTNHost from, DTNHost to) {

    }

    @Override
    public void messageDeleted(Message m, DTNHost where, boolean dropped) {

    }

    @Override
    public void messageTransferAborted(Message m, DTNHost from, DTNHost to) {

    }

    @Override
    public void messageSaveToBuffer(Message m, DTNHost to) {

    }

    @Override
    public void connectionUp(DTNHost thisHost) {}


}
