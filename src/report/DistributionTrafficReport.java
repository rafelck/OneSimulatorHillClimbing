package report;

import core.DTNHost;
import core.Message;
import core.MessageListener;
import core.Settings;


public class DistributionTrafficReport extends Report implements MessageListener {

    private int nrofRelayed;
    private int nrofStarted;
    private int nrofSavedInBuffer;

    public DistributionTrafficReport() {
        init();
    }
    @Override
    protected void init() {
        super.init();
        nrofRelayed = 0;
        nrofSavedInBuffer = 0;
    }

    @Override
    public void newMessage(Message m) {


    }

    @Override
    public void messageTransferStarted(Message m, DTNHost from, DTNHost to) {
        nrofStarted++;

    }

    @Override
    public void messageDeleted(Message m, DTNHost where, boolean dropped) {

    }

    @Override
    public void messageTransferAborted(Message m, DTNHost from, DTNHost to) {

    }

    @Override
    public void messageTransferred(Message m, DTNHost from, DTNHost to, boolean firstDelivery) {

    }

    @Override
    public void messageSaveToBuffer(Message m, DTNHost to) {
        nrofSavedInBuffer++;

    }

    @Override
    public void done()
    {
        Settings s = new Settings();
        int nrofNode = s.getInt("Group.nrofHosts");
        String report = "";

        report += "\nTransmission Load         = "+nrofStarted/nrofNode+"\n"
                + "Resource Load             = "+nrofSavedInBuffer/nrofNode;
        write(report);
        super.done();
    }

    @Override
    public void connectionUp(DTNHost thisHost) {}
}
